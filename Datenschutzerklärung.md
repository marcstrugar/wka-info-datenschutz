Datenschutzerklärung

Zugriffsrechte der App

Kamera

Für eine vollständige Nutzung der App sind Rechte für die Kameranutzung erforderlich.
Die Daten der Kamerabilder werden nicht gespeichert und nicht an dritte weiter gegeben.
Alle Daten werden lokal auf ihrem dem Gerät verarbeitet und auf keinen Web-Server hochgeladen.

Standort

Für eine vollständige Nutzung der App sind Rechte für die Standortabfrage erforderlich.
Standortdaten werden nicht gespeichert und nicht an dritte weiter gegeben.
Alle Daten werden lokal auf ihrem dem Gerät verarbeitet und auf keinen Web-Server hochgeladen.

Speicher

Für eine vollständige Nutzung der App sind Rechte für den Zugriff auf den Speicher erforderlich.
Es werden keine Daten aus ihrem Speicher an dritte weitergegeben.
Alle Daten werden lokal auf ihrem dem Gerät verarbeitet und auf keinen Web-Server hochgeladen.


Widerspruch gegen Datenerhebungen der Zugriffe aus der App

Sie können die Berechtigungen der App ablehnen. Nur wenn Sie explizit den einzelnen Nutzungen zustimmen werden
entsprechende Zugriffe auf Speicher, Kamera und Standort durchgeführt.

Google Maps Datenschutzerklärung

Wir benutzen in unserer App Google Maps der Firma Google Inc. Für den europäischen Raum ist das Unternehmen Google 
Ireland Limited (Gordon House, Barrow Street Dublin 4, Irland) für alle Google-Dienste verantwortlich. 
Mit Google Maps können wir Ihnen Standorte besser zeigen und damit unser Service an Ihre Bedürfnisse anpassen. 
Durch die Verwendung von Google Maps werden Daten an Google übertragen und auf den Google-Servern gespeichert. 
Hier wollen wir nun genauer darauf eingehen, was Google Maps ist, warum wir diesen Google-Dienst in Anspruch nehmen, 
welche Daten gespeichert werden und wie Sie dies unterbinden können.

Was ist Google Maps?
Google Maps ist ein Internet-Kartendienst der Firma Google. Mit Google Maps können Sie online über einen PC, ein Tablet 
oder eine App genaue Standorte von Städten, Sehenswürdigkeiten, Unterkünften oder Unternehmen suchen. Wenn Unternehmen auf 
Google My Business vertreten sind, werden neben dem Standort noch weitere Informationen über die Firma angezeigt. 
Um die Anfahrtsmöglichkeit anzuzeigen, können Kartenausschnitte eines Standorts mittels HTML-Code in eine Website eingebunden werden. 
Google Maps zeigt die Erdoberfläche als Straßenkarte oder als Luft- bzw. Satellitenbild. Dank der Street View Bilder und den 
hochwertigen Satellitenbildern sind sehr genaue Darstellungen möglich.

Welche Daten werden von Google Maps gespeichert?

Eine genauere Einsicht in die von Google erhobenen Daten kann über diesen Link erfolgen:
https://policies.google.com/privacy?hl=de
Hier erfahren Sie zudem wie lange Daten gespeichert werden und wie Sie Daten löschen können.


Kontaktdaten des Entwicklers
Technische Hochschule Lübeck
Torben Friedrich Görner
torbengrn97@gmail.com
